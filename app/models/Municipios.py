from app import db

class Municipios(db.Model):
    __table_name__='muncipios'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(100))
    municipio = db.Column(db.String(150))
    altitud = db.Column(db.String(2))
    tipo = db.Column(db.String(2))

    def __repr__(self):
        return '<Municipio {}>'.format(self.municipio)