from app import db

class TipoEmbarazo(db.Model):
    __table_SQLALCHEMY_DATABASE_URIname__='tipo_embarazo'

    id = db.Column(db.Integer, primary_key=True)
    codigo =  db.Column(db.String(10))
    tipo_embarazo = db.Column(db.String(150))

    def __repr__(self):
        return '<TipoEmbarazo {}>'.format(self.tipo_embarazo)