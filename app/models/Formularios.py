from app import db

class Formularios(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    
    departamento_id = db.Column(db.Integer, db.ForeignKey('departamentos.id'), nullable = False)
    departamento = db.relationship('Departamentos', backref = db.backref('formularios', lazy = True))
    
    municipio_id = db.Column(db.Integer, db.ForeignKey('municipios.id'), nullable = False)
    municipio = db.relationship('Municipios', backref = db.backref('formularios', lazy = True))

    nivel_educativo_id = db.Column(db.Integer, db.ForeignKey('nivel_educativo.id'), nullable = False)
    nivel_educativo = db.relationship('NivelEducativo', backref = db.backref('formularios', lazy = True))
    
    seguridad_social_id = db.Column(db.Integer, db.ForeignKey('seguridad_social.id'), nullable = False)
    seguridad_social = db.relationship('SeguridadSocial', backref = db.backref('formularios', lazy = True))

    tipo_embarazo_id = db.Column(db.Integer, db.ForeignKey('tipo_embarazo.id'), nullable = False)
    tipo_embarazo = db.relationship('TipoEmbarazo', backref = db.backref('formularios', lazy = True))

    estado_civil_id = db.Column(db.Integer, db.ForeignKey('estado_civil.id'), nullable = False)
    estado_civil = db.relationship('EstadoCivil', backref = db.backref('formularios', lazy = True))
    
    numero_hijos =  db.Column(db.Integer);

    numero_hijos_muertos = db.Column(db.Integer)

    email_medico = db.Column(db.String(100))

    tiempo_gestacion = db.Column(db.String(10))
     
    def __repr__(self):
        return '<Formularios %r>' % self.title

