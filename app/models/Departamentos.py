from app import db

class Departamentos(db.Model):
    __table_name__='departamentos'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(100))
    departamento = db.Column(db.String(150))
    region_id = db.Column(db.Integer) 
    desarrollado = db.Column(db.String(1))
    
    def __repr__(self):
        return '<Departamento {}>'.format(self.departamento)