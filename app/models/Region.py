from app import db

class Region(db.Model):
    __table_name__='region'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(100))
    nombre_region = db.Column(db.String(150))
    
    def __repr__(self):
        return '<Region {}>'.format(self.nombre_region)