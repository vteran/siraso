from app import db

class EstadoCivil(db.Model):
    __table_name__='estado_civil'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.Text(10))
    estado_civil = db.Column(db.String(150))

    def __repr__(self):
        return '<EstadoCivil {}>'.format(self.departamento)