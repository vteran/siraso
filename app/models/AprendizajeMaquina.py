from app import db

class AprendizajeMaquina(db.Model):
    __table_name__='aprendizaje_maquina'
    id = db.Column(db.Integer, primary_key=True)
    estado = db.Column(db.Boolean)
    nombre_modelo = db.Column(db.Text)
    pickle_data=  db.Column(db.PickleType)

    def __repr__(self):
        return '<Modelo %s>' % self.nombre_modelo
