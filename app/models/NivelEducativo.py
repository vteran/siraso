from app import db

class NivelEducativo(db.Model):
    __table_name__='nivel_educativo'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.String(100))
    nivel = db.Column(db.String(150))

    def __repr__(self):
        return '<NivelEducativo {}>'.format(self.nivel)