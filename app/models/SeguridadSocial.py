from app import db

class SeguridadSocial(db.Model):
    __table_name__='seguridad_social'

    id = db.Column(db.Integer, primary_key=True)
    codigo = db.Column(db.Text)
    seguridad = db.Column(db.String(150))

    def __repr__(self):
        return '<SeguridadSocial {}>'.format(self.seguridad)