import os
import logging
from flask import Flask
from config import config
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
db = SQLAlchemy() 
mail= Mail()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    
    if test_config is None:
        # load the instance config, if it exists, when not testing
        
        app.config.from_object(config['production'])
        db.init_app(app)
        db.app = app
        mail.init_app(app)
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_object(config[test_config])

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from controllers import bienvenida, registro, resultado
    from models import Departamentos, EstadoCivil, Municipios, NivelEducativo, SeguridadSocial, TipoEmbarazo, Formularios
    app.register_blueprint(bienvenida.bp)
    app.register_blueprint(registro.bp)
    app.register_blueprint(resultado.bp)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    
    app.logger.addHandler(stream_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info('Microblog startup')

    return app


