import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = True
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = ''
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://homestead:secret@localhost:54320/siraso'

    #MAIL_CONFIGURATION
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = 1
    MAIL_USERNAME = 'elalcon89@gmail.com'
    MAIL_PASSWORD = '$amuRa1Kk'
    ADMINS = ['vteran93@yahoo.es']
    
    #Flasky copied variables
    FLASKY_MAIL_SUBJECT_PREFIX='siraso'
    FLASKY_MAIL_SENDER = 'Siraso Admin <admin@siraso.org>'

class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://bjzmwssgbvoesf:ec29c45a4188a1710159e09ba385ec153b0a6d0857690032aa74c34245fad47f@ec2-174-129-18-247.compute-1.amazonaws.com:5432/df314l8ofjkfj6'
    #MAIL_CONFIGURATION
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = 1
    MAIL_USERNAME = 'elalcon89@gmail.com'
    MAIL_PASSWORD = '$amuRa1Kk'
    ADMINS = ['vteran93@yahoo.es']
    

class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}