from flask import render_template
import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
bp = Blueprint('bienvenida', __name__, url_prefix='/home')
class Bienvenida:

    __template='welcome.html'
    
    def __init__(self):
        pass

    def get(self):
        return render_template(self.__template)


@bp.route('/bienvenida', methods=['GET', 'POST'])
def bienvenida():
    if request.method=='GET':
        b = Bienvenida()
        return b.get()