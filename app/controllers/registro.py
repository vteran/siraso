import datetime
from flask import render_template
import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from app import db
from ..models.Region import Region
from ..models.Municipios import Municipios
from ..models.Formularios import Formularios
from ..models.EstadoCivil import EstadoCivil
from ..models.TipoEmbarazo import TipoEmbarazo
from ..models.Departamentos import Departamentos
from ..lib.MachineLearning  import MachineLearning
from ..lib.email  import send_email
from ..models.NivelEducativo import NivelEducativo
from ..models.SeguridadSocial import SeguridadSocial
from ..models.AprendizajeMaquina import AprendizajeMaquina

bp = Blueprint('registro', __name__, url_prefix='/home')
class Registro:

    __template='registro.html'
    
    def __init__(self):
        pass

    def get(self):
        return render_template(self.__template)

    def post(self):
        
        data = self.__mapperFormToDataBase(request.form)
        registro = Formularios(departamento_id = data['departamento_id']
            , municipio_id = data['municipio_id']
            , nivel_educativo_id = data['nivel_educativo_id']
            , seguridad_social_id = data['seguridad_social_id']
            , tipo_embarazo_id = data['tipo_embarazo_id']
            , estado_civil_id = data['estado_civil_id'])
        registro.numero_hijos = request.form['numHijos']
        registro.numero_hijos_muertos = request.form['numHijosFallecidos']
        registro.tiempo_gestacion = request.form['tiempoGestacion']
        db.session.add(registro)
        db.session.commit()
        predictedResult = self.__sendToModel(request.form, data)
        self.__send_email(predictedResult, request.form['emailMedico'])
        return redirect('/home/resultado')

    def __sendToModel(self, form, data):
        mapping = self.__mapperFormToSkeLearnModel(form, data)
        machine = MachineLearning(AprendizajeMaquina, mapping)
        return machine.predict()

    def __mapperFormToSkeLearnModel(self, form, data):
        
        mapping=[
            ('MES',datetime.datetime.now().month),
            ('REGION', Region.query.filter_by(id = Departamentos.query.filter_by(id=data['departamento_id']).first().region_id).first().codigo),
            ('DESARROLLO_DEP',Departamentos.query.filter_by(id=data['departamento_id']).first().desarrollado), # pendiente
            ('ALTITUD_MNCP', Municipios.query.filter_by(codigo= form['municipio']).first().altitud),
            ('RES_MADRE',form['resMadre']),
            ('SEGURIDAD SOCIAL',SeguridadSocial.query.filter_by(codigo=form['seguridadSocial']).first().codigo),
            #('RANKING EPS',5),#remover del modelo
            ('TIPO_EMBARAZO',form['tipoEmbarazo']),
            ('TIEMPO_GESTACION',form['tiempoGestacion']),
            ('EDAD_MADRE',form["edadMadre"]),
            ('N_HIJOSV',form['numHijos']), 
            ('N_HIJOSM',form['numHijosFallecidos']),
            ('ESTADO CIVIL', EstadoCivil.query.filter_by(codigo=form['seguridadSocial']).first().codigo),
            ('NIVEL EDUCATIVO', NivelEducativo.query.filter_by(codigo=form['nivelEducativo']).first().codigo),
            ('ULTCURMAD',form['ultCursado'])
        ]
        return mapping


    def __mapperFormToDataBase(self, form):
        
        departamento_id =  Departamentos.query.filter_by(codigo=form['departamento'] ).first().id 
        municipio_id= Municipios.query.filter_by(codigo= form['municipio']).first().id
        tipo_embarazo_id = TipoEmbarazo.query.filter_by(codigo=form['tipoEmbarazo']).first().id
        seguridad_social_id=SeguridadSocial.query.filter_by(codigo=form['seguridadSocial']).first().id
        estado_civil_id = EstadoCivil.query.filter_by(codigo=form['seguridadSocial']).first().id
        nivel_educativo_id = NivelEducativo.query.filter_by(codigo=form['nivelEducativo']).first().id
        return {'departamento_id': departamento_id
            , 'municipio_id': municipio_id
            , 'nivel_educativo_id':nivel_educativo_id
            , 'tipo_embarazo_id': tipo_embarazo_id
            , 'seguridad_social_id':seguridad_social_id
            , 'estado_civil_id': estado_civil_id
            }

    def __send_email(self, resultado, correo):
        if(len(resultado)>0):
            if resultado[0] > 0:
                diagnostico='EN RIESGO'
            else:
                diagnostico='FUERA DE RIESGO'
        else:
            diagnostico='Error'
        
        send_email(correo,  "Sistema Riesgo Psicosocial", 'mail_notification', email=correo, resultado=diagnostico )
        
        


@bp.route('/registro', methods=['GET', 'POST'])
def registro():
    b = Registro()
    if request.method=='GET':
        return b.get()
    return b.post()