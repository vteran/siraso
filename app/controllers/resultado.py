from flask import render_template
import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
bp = Blueprint('resultado', __name__, url_prefix='/home')
class Resultado:

    __template='result.html'
    
    def __init__(self):
        pass

    def get(self):
        return render_template(self.__template)


@bp.route('/resultado', methods=['GET',])
def resultado():
    if request.method=='GET':
        b = Resultado()
        return b.get()