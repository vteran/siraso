import pickle

class MachineLearning:
    
    def __init__(self, databaseModel, values):
        self.databaseModel = databaseModel
        self.values = self.__setStructure(values)

    def predict(self):
        skLearnModel = self.__getModel()
        return skLearnModel.predict(self.values)


    def __getModel(self):
        binary_response = self.databaseModel.query.filter_by(estado=True).first().pickle_data
        skLearnModel = binary_response
        return skLearnModel

    def __setStructure(self,values):
        testvalues=[]
        for v in values:
            try:
                testvalues.append(float(v[1]))
            except TypeError:
                testvalues.append(float(-999))
            except ValueError:
                testvalues.append(float(-999))
        return [testvalues]

    