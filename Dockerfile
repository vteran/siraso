FROM heroku/miniconda

ADD requirements.txt /tmp/requirements.txt

RUN pip install --upgrade pip

RUN pip install -r /tmp/requirements.txt

ADD ./app /opt/siraso/app

ADD ./__init__.py /opt/siraso/

ADD ./_continuous_distns.py /opt/siraso/

ADD ./siras.py /opt/siraso/

ADD ./runtime.txt /opt/siraso/

WORKDIR /opt/siraso